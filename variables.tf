variable "policy_name" {
  default     = "enforce-mfa"
  description = "The name of the policy."
  type        = string
}

variable "path" {
  default     = "/"
  description = "Path in which to create the policy. (Optional, default '/')"
  type        = string
}

variable "groups" {
  description = "Enforce MFA for the members in these groups"
  type        = list(string)
  default     = []
}

variable "users" {
  description = "Enforce MFA for these users"
  type        = list(string)
  default     = []
}
