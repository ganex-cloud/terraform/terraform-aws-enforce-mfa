# AWS MFA Terraform module

Terraform module to enforce MFA for AWS groups and users.

This module enforce users or groups to enable MFA, without MFA all resources are denied.


## Usage

```tf

resource "aws_iam_group" "mfa_group" {
  name = "MFAGroup"
}

resource "aws_iam_user" "mfa_user" {
  name = "MFAUser"
}

module "aws-enforce-mfa" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-enforce-mfa.git?ref=0.12"
  groups = [aws_iam_group.mfa_group.name]
  users  = [aws_iam_user.mfa_user.name]
}
```

## Module input variables

- `policy_name` Policy name (Default: _enforce-mfa_)
- `path` Policy path (Default: _/_)
- `groups` Enforce MFA for the members in these groups (Default: _[]_)
- `users` Enforce MFA for these users (Default: _[]_)

## License

MIT licensed. See LICENSE for full details.